<?php

//extend php unit test frame work
class RobotTest extends \PHPUnit\Framework\TestCase
{
	public function testTrueAssertsToTrue()
  {
     $this->assertTrue(true);
  }

  	public function testRobotMoveRight()
  	{
  		$robot = new \App\Src\Robot;
  		$robot->moveRight();
  		$this->assertEquals($robot->getDir(),"East");
  	}

  		public function testRobotMoveLeft()
  	{
  		$robot = new \App\Src\Robot;
  		$robot->moveLeft();
  		$this->assertEquals($robot->getDir(),"West");
  	}


  	public function testRobotIntialDirectionToNorth()
  	{
  		$robot = new \App\Src\Robot;
  		$this->assertEquals($robot->getDir(),"North");
  	}

  	public function testRobotIntialXAxisIsZero()
  	{
  		$robot = new \App\Src\Robot;
  		$this->assertEquals($robot->getX(),0);
  	}

  	public function testRobotIntialYAxisIsZero()
  	{
  		$robot = new \App\Src\Robot;
  		$this->assertEquals($robot->getY(),0);
  	}
}