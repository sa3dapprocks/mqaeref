<?php

namespace App\Src;

interface RobotInterface{

  public function moveRight();
  public function moveLeft();
  public function walk(int $steps);
  public function getX();
  public function getY();
  public function getDir();

}
