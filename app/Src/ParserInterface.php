<?php

namespace App\Src;

interface ParserInterface{
    public function parse(string $input);
    public function getTokens() :array;
}
