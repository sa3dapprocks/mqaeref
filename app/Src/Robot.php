<?php

namespace App\Src;

use \App\Controller;
use \App\Controller\WalkingController;
use \App\Controller\PostionController;
use \App\Controller\Parser;

class Robot implements RobotInterface{

    private $x_axis=0;
    private $y_axis=0;
    private $dir=0;
    private $orders = array();
    const North = 0;
    const East = 1;
    const South = 2;
    const West = 3;
    
    public function moveRight()
    {

            if (0<=$this->dir && $this->dir<=2) {
                $this->dir = $this->dir+1;
            } else {
                $this->dir = 0;
              }
    }         

    public function moveLeft()
    {
        if ($this->dir>0) {
            $this->dir-1;
        } else {
            $this->dir = 3;
        }
    }

    public function walk2(int $steps)
    {
        if ($this->dir==0 || $this->dir==2) {
            $this->y_axis = $this->y_axis+1;    
        } else {
            $this->x_axis = $this->x_axis+1;
        }

    }

    public function walk(int $steps)
    {
         switch ($this->dir) {
            case self::North:
               return "North";
               break;

            case self::East:
                return "East";
                break;

            case self::South:
                return "South";
                break;

            case self::West:
                return "West";
                break;
            default:
                return "Lost";
                break;
        }

    }

    public function getX()
    {
      return $this->x_axis;
    }

    public function getY() 
    {
      return $this->y_axis;
    }

    public function getDir() 
    {
        switch ($this->dir) {
            case self::North:
               return "North";
               break;

            case self::East:
                return "East";
                break;

            case self::South:
                return "South";
                break;

            case self::West:
                return "West";
                break;
            default:
                return "Lost";
                break;
        }

    }
}
