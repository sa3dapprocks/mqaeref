<?php

namespace App\Src;

class Parser implements ParserInterface{

    private $token;

	public function parse(string $input)
	{
        $flag = PREG_SET_ORDER;
    	preg_match_all('/R|L|W|.+/i',trim($input),$tokens,$flag);
    	$this->token = $tokens;
	}

	public function getTokens() :array
	{
        return $this->token;
	}

}
