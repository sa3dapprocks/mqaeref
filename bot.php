<?php

require_once 'app/start.php';

use \App\Src\Parser;
use \App\Src\Robot;

$myBot = new Robot;
$parser = new Parser;
$parser->parse($argv[1]);
$orders = $parser->getTokens();

foreach($orders as $order) {

    switch ($order[0]) {
        case 'R':
            $myBot->moveRight();
      		break;

        case 'L':
          	$myBot->moveLeft();
          	break;

        case 'W':
          	$myBot->walk(1);
          	break;
    }
}

echo "X: ".$myBot->getX()." Y: ".$myBot->getY()." Direction: ".$myBot->getDir();

?>
